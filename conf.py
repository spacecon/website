from __future__ import annotations

from dataclasses import dataclass
from enum import Enum
from pathlib import Path
from shutil import which
from typing import Any, Sequence

import pandas as pd

# :: Path ::
CONF_FILE = Path(__file__)
CWD = CONF_FILE.parent

LAYOUT_PATH = CWD / "layout"
OUTPUT_PATH = CWD / "public"
ASSETS_PATH = CWD / "assets"
NODE_MODULES_PATH = CWD / "node_modules"

# Sync folders in node_modules into folders in assets
NODE_MODULES_SYNC = {NODE_MODULES_PATH / "@fortawesome" / "fontawesome-free": "fa"}

NPX = which("npx")

if NPX is None:
    raise ValueError("Couldn't find NPX_CMD!")

# :: Options ::
TAILWIND_CONFIG_FILE = CWD / "tailwind.config.js"


@dataclass(frozen=True)
class Url:
    href: str
    title: str


@dataclass(frozen=True)
class Image:
    src: str
    alt: str


@dataclass(frozen=True)
class MenuItem:
    title: str
    url: Url | None = None
    icon: str | None = None
    children: list[MenuItem] | None = None

    @property
    def id(self) -> str:
        return self.title.lower()


# Menus


@dataclass(frozen=True)
class SponsorLevel:
    name: str
    color: str


@dataclass(frozen=True)
class SponsorItem:
    logo: Image
    url: Url


def make_program(events: Sequence[ProgramEvent]) -> pd.DataFrame:
    _df = pd.DataFrame(events).set_index("time")
    _df.room = _df.room.apply(lambda x: x.value)

    # Last row is special
    df = _df.iloc[:-1].copy()

    def _fill(x):
        to_add = df.index[(df.index <= x.end_time) & (df.index >= x.name)].unique()

        if len(row := x.to_frame().T.reindex(to_add).ffill()) > 1:
            return row[:-1]
        else:
            return row

    extended_df = pd.concat(row for row in df.apply(_fill, axis=1))

    # Copy last day that got dropped
    extended_df = pd.concat((extended_df, _df.iloc[-1].to_frame().T))
    extended_df.loc[extended_df.index[-1], "end_time"] = extended_df.iloc[-1].name

    # Time instead of Timestamp
    extended_df.index = pd.Index(
        extended_df.index.to_series().apply(lambda x: x.time().strftime("%H:%M")),
        name="time",
    )
    extended_df.end_time = extended_df.end_time.apply(
        lambda x: x.time().strftime("%H:%M")
    )

    return extended_df.set_index("room", append=True).sort_values(by="time")


class Room(Enum):
    MAISON = "Maison d'Italie, Cité Universitaire, 7A boulevard Jourdan 75014 Paris"
    GRAND_HALL = "Grand Hall"
    SALON = "Salon d'honneur"
    AMPHI = "Amphi Poincaré"
    PITCH = "SpaceConcept Room"
    PC1 = "Small Room 1"
    PC2 = "Small Room 2"
    OUTDOOR = "Outdoor"


@dataclass
class LinenupItem:
    full_name: str
    affiliation: str
    img: str | None = None


def unwrap_lineup(item: LinenupItem) -> str:
    return f"{item.full_name} ({item.affiliation})"


@dataclass
class ProgramEvent:
    time: pd.Timestamp
    title: str
    end_time: pd.Timestamp | None = None
    room: Room | None = None
    speaker: str | None = None


global_ctx = {
    "LANG": "en",
    "KSP_FORM_ENTRYPOINT": "https://ks-subscribe.rdb-c41.workers.dev",
}

dev_ctx_override = {"KSP_FORM_ENTRYPOINT": "http://localhost:8787"}

lineup = {
    "chaubard": LinenupItem(
        full_name="Laura Chaubard",
        affiliation="Acting President and Director General of Ecole Polytechnique",
        img="Laura_Chaubard.webp",
    ),
    "baptiste": LinenupItem(
        full_name="Philippe Baptiste",
        affiliation="CNES President",
        img="Philippe_Baptiste.webp",
    ),
    "wagner": LinenupItem(
        full_name="Alain Wagner",
        affiliation="Vice-President Marketing & Sales - Head of Institutional Space Business, Airbus Defence and Space",
        img="Alain_Wagner.webp",
    ),
    "naja": LinenupItem(
        full_name="Geraldine Naja",
        affiliation="Director of Commercialization, Industry, and Competitiveness at ESA",
        img="Geraldine_Naja.webp",
    ),
    "saccoccia": LinenupItem(
        full_name="Giorgio Saccoccia",
        affiliation="Former ASI President, Senior Advisor to the director general ESA",
        img="Giorgio_Saccoccia.webp",
    ),
    "baratte": LinenupItem(
        full_name="Bertrand Baratte",
        affiliation="Space Market Director, Airliquide, EURO2MOON, General Secretary",
        img="Bertrand_Baratte.webp",
    ),
    "bucciantini": LinenupItem(
        full_name="Luca Bucciantini",
        affiliation="Technical Director of the École polytechnique Space Center",
        img="Luca_Bucciantini.webp",
    ),
    "decadi": LinenupItem(
        full_name="Aline Decadi",
        affiliation="Ariane 6 Launch System Engineer ESA",
        img="Aline_Decadi.webp",
    ),
    "lowne": LinenupItem(
        full_name="Matt Lowne",
        affiliation="Youtuber, Space Content Creator (@Matt_Lowne)",
        img="Matt_Lowne.webp",
    ),
    "fleischer": LinenupItem(
        full_name="Louise Fleischer",
        affiliation="The Spring Institute for Forests on the Moon",
        img="Louise_Fleischer.webp",
    ),
    "chevrier": LinenupItem(
        full_name="Raphael Chevrier",
        affiliation="Head of Communication Maia Space",
        img="Raphael_Chevrier.webp",
    ),
    "jerosme": LinenupItem(
        full_name="Whitney Jerosme",
        affiliation="Youtuber, Space Content Creator (@SpaceExplorerW)",
        img="Whitney_Jerosme.webp",
    ),
    "maximin": LinenupItem(
        full_name="Stanislas Maximin",
        affiliation="CEO Latitude",
        img="Stanislas_Maximin.webp",
    ),
    "garnier": LinenupItem(
        full_name="Thomas Garnier",
        affiliation="Space Education Specialist Universite Paris-Saclay",
        img="Thomas_Garnier.webp",
    ),
    "jercaianu": LinenupItem(
        full_name="Alexandra Jercaianu",
        affiliation="SGAC Diversity and Gender Equality PG, Sponsorship Co-lead #OurGiantLeap",
        img="Alexandra_Jercaianu.webp",
    ),
    "clerbaux": LinenupItem(
        full_name="Cathy Clerbaux",
        affiliation="Senior scientist at Laboratoire Atmosphères & Observations Spatiales",
        img="Cathy_Clerbaux.webp",
    ),
    "lacaze": LinenupItem(
        full_name="Guilhem Lacaze",
        affiliation="Sustainable power | ex-SpaceX",
        img="Guilhem_Lacaze.webp",
    ),
    "smidtas": LinenupItem(
        full_name="Eva Smidtas",
        affiliation="AstroPanthéon Team",
        img="Eva_Smidtas.webp",
    ),
    "polino": LinenupItem(
        full_name="Mario Polino",
        affiliation="Assistant Professor at PoliMi Computer Security, capitain of the CTF team mhackeroni",
        img="Mario_Polino.webp",
    ),
    "senechal": LinenupItem(
        full_name="Lucie Senechal-Perrouault",
        affiliation="PhD Candidate CNRS",
        img="Lucie_Senechal_Perrouault.webp",
    ),
    "martinez": LinenupItem(
        full_name="Susie Martinez",
        affiliation="Science Communicator - AdAstraSu, Payload Engineer in the Aerospace Industry",
        img="Susie_Martinez.webp",
    ),
    "roettgen": LinenupItem(
        full_name="Raphael Roettgen",
        affiliation="Governing member ISU and lecturer in EPFL",
        img="Raphael_Roettgen.webp",
    ),
    "callens": LinenupItem(
        full_name="Natacha Callens",
        affiliation="Administrator ESA Academy- Engagement Program",
        img="Natacha_Callens.webp",
    ),
    "weiss": LinenupItem(
        full_name="Peter Weiss", affiliation="CEO Spartan Space", img="Peter_Weiss.webp"
    ),
    "nassey": LinenupItem(
        full_name="Charlotte Nassey",
        affiliation="Government Affairs Officer for ispace Europe",
        img="Charlotte_Nassey.webp",
    ),
    "pallu": LinenupItem(
        full_name="Eric-Olivier Pallu",
        affiliation="Senior Policy Officer - EU Commission",
        img="Eric_Olivier_Pallu.webp",
    ),
    "lacaille": LinenupItem(
        full_name="Lacaille Mélissandre",
        affiliation="Digital content strategist / Aerospace communicator",
        img="Melissandre_Lacaille.webp",
    ),
    "looney": LinenupItem(
        full_name="Caeley Looney",
        affiliation="CEO of Reinvented Inc. and Mission Operations Engineer for Commercial Lunar Lander Program",
        img="Caeley_Looney.webp",
    ),
    "savage": LinenupItem(
        full_name="Lauren Savage",
        affiliation="Lunar Spacecraft Structural Analysis & Testing Engineer in the Commercial Space Industry",
        img="Lauren_Savage.webp",
    ),
    "siraudin": LinenupItem(
        full_name="Arnaud Siraudin",
        affiliation="Associate Director Arthur D. Little",
        img="Arnaud_Siraudin.webp",
    ),
    "guyomarch": LinenupItem(
        full_name="Alban Guyomarch",
        affiliation="Coordinator of the Destination Moon Working Group at ANRT and Phd Candidate in Space Law",
        img="Alban_Guyomarch.webp",
    ),
}


# Pages
index = {
    "DESCRIPTION": (
        "SpaceCon® is an inclusive event to have fun "
        "learn and celebrate talking about space."
    ),
    "TITLE": "SpaceCon 2023 | Live Space to the fullest !",
    "program": {
        "16": make_program(
            [
                ProgramEvent(
                    time=pd.Timestamp("19:00"),
                    end_time=pd.Timestamp("19:30"),
                    room=Room.MAISON,
                    title="Introduction",
                    speaker="SpaceCon, Alumni PoliMi Parigi, Alumni PoliTo Paris",
                ),
                ProgramEvent(
                    time=pd.Timestamp("19:30"),
                    end_time=pd.Timestamp("21:00"),
                    room=Room.MAISON,
                    title="Opening Cocktail in partnership with the associations of Alumni Politecnico di Milano, Alumni Politecnico di Torino",
                ),
                ProgramEvent(
                    time=pd.Timestamp("21:00"),
                    end_time=pd.Timestamp("21:00"),
                    room=Room.MAISON,
                    title="End of the cocktail",
                ),
            ]
        ),
        "17": make_program(
            [
                ProgramEvent(
                    time=pd.Timestamp("08:45"),
                    end_time=pd.Timestamp("09:00"),
                    room=Room.GRAND_HALL,
                    title="Registration",
                    speaker="SpaceCon Team",
                ),
                ProgramEvent(
                    time=pd.Timestamp("09:00"),
                    end_time=pd.Timestamp("09:05"),
                    room=Room.AMPHI,
                    title="Opening",
                    speaker="SpaceCon Team",
                ),
                ProgramEvent(
                    time=pd.Timestamp("09:05"),
                    end_time=pd.Timestamp("09:15"),
                    room=Room.AMPHI,
                    title="Honored Guest Talk",
                    speaker=f"{lineup['chaubard'].full_name} ({lineup['chaubard'].affiliation})",
                ),
                ProgramEvent(
                    time=pd.Timestamp("09:15"),
                    end_time=pd.Timestamp("09:30"),
                    room=Room.AMPHI,
                    title="Honored Guest Talk",
                    speaker=f"{lineup['baptiste'].full_name} ({lineup['baptiste'].affiliation})",
                ),
                ProgramEvent(
                    time=pd.Timestamp("09:30"),
                    end_time=pd.Timestamp("09:45"),
                    room=Room.AMPHI,
                    title="Honored Guest talk",
                    speaker=f"{lineup['saccoccia'].full_name} ({lineup['saccoccia'].affiliation})",
                ),
                ProgramEvent(
                    time=pd.Timestamp("09:45"),
                    end_time=pd.Timestamp("10:00"),
                    room=Room.AMPHI,
                    title="From SpaceX to Earth",
                    speaker=f'{unwrap_lineup(lineup["lacaze"])}',
                ),
                ProgramEvent(
                    time=pd.Timestamp("10:00"),
                    end_time=pd.Timestamp("11:00"),
                    room=Room.GRAND_HALL,
                    title="Coffee Break",
                ),
                ProgramEvent(
                    time=pd.Timestamp("11:00"),
                    end_time=pd.Timestamp("12:00"),
                    room=Room.AMPHI,
                    title="Space Education: What are we doing to get the new generations ready for the challenges of the future in space",
                    speaker=", ".join(
                        (
                            unwrap_lineup(lineup[item])
                            for item in [
                                "garnier",
                                "bucciantini",
                                "roettgen",
                                "callens",
                            ]
                        )
                    ),
                ),
                ProgramEvent(
                    time=pd.Timestamp("12:00"),
                    end_time=pd.Timestamp("13:45"),
                    room=Room.GRAND_HALL,
                    title="L<sup>a</sup>unch Break | SpaceUp activity",
                ),
                ProgramEvent(
                    time=pd.Timestamp("13:45"),
                    end_time=pd.Timestamp("14:00"),
                    room=Room.AMPHI,
                    speaker=f'{unwrap_lineup(lineup["naja"])}',
                    title="Managing a Directorate Focusing on Competitiveness at the European Space Agency – A Story",
                ),
                ProgramEvent(
                    time=pd.Timestamp("14:00"),
                    end_time=pd.Timestamp("14:15"),
                    room=Room.AMPHI,
                    title="How the EIC builds success stories for newspace SMEs and startups",
                    speaker=unwrap_lineup(lineup["pallu"]),
                ),
                ProgramEvent(
                    time=pd.Timestamp("14:15"),
                    end_time=pd.Timestamp("14:30"),
                    room=Room.AMPHI,
                    title="Keynote",
                    speaker=unwrap_lineup(lineup["fleischer"]),
                ),
                ProgramEvent(
                    time=pd.Timestamp("14:30"),
                    end_time=pd.Timestamp("15:30"),
                    room=Room.AMPHI,
                    title="The Moon, cornerstone of sustainability in space for Earth and Space?",
                    speaker=", ".join(
                        (
                            unwrap_lineup(lineup[item])
                            for item in [
                                "wagner",
                                "nassey",
                                "siraudin",
                                "baratte",
                                "weiss",
                                "guyomarch",
                            ]
                        )
                    ),
                ),
                ProgramEvent(
                    time=pd.Timestamp("15:30"),
                    end_time=pd.Timestamp("16:15"),
                    room=Room.GRAND_HALL,
                    title="Break | SpaceUp Activity",
                ),
                ProgramEvent(
                    time=pd.Timestamp("16:15"),
                    end_time=pd.Timestamp("16:30"),
                    room=Room.AMPHI,
                    speaker=f'{lineup["senechal"].full_name} ({lineup["senechal"].affiliation})',
                    title="CNRS-EHESS PhD candidate on Chinese commercial space in the national innovation system",
                ),
                ProgramEvent(
                    time=pd.Timestamp("16:30"),
                    end_time=pd.Timestamp("16:45"),
                    room=Room.AMPHI,
                    speaker=f'{lineup["clerbaux"].full_name} ({lineup["clerbaux"].affiliation})',
                    title="Ammonia hotspots revealed from space",
                ),
                ProgramEvent(
                    time=pd.Timestamp("16:45"),
                    end_time=pd.Timestamp("17:45"),
                    room=Room.AMPHI,
                    title="Fireside chat by Reinvented Magazine",
                    speaker=", ".join(
                        (
                            unwrap_lineup(lineup[item])
                            for item in ["looney", "martinez", "savage", "lacaille"]
                        )
                    ),
                ),
                ProgramEvent(
                    time=pd.Timestamp("17:45"),
                    end_time=pd.Timestamp("18:00"),
                    room=Room.AMPHI,
                    title="High School, ESA Competition and rockets",
                    speaker=unwrap_lineup(lineup["smidtas"]),
                ),
                ProgramEvent(
                    time=pd.Timestamp("18:00"),
                    end_time=pd.Timestamp("18:15"),
                    room=Room.AMPHI,
                    title="'Hack-a-sat-4': Hacking a Satellite for Fun and Profit $$$",
                    speaker=f'{lineup["polino"].full_name} ({lineup["polino"].affiliation})',
                ),
                ProgramEvent(
                    time=pd.Timestamp("18:15"),
                    end_time=pd.Timestamp("18:30"),
                    room=Room.AMPHI,
                    title="Closing",
                    speaker="SpaceCon Team",
                ),
                ProgramEvent(
                    time=pd.Timestamp("18:30"), title="End of Day", room=Room.GRAND_HALL
                ),
                ProgramEvent(
                    time=pd.Timestamp("20:00"),
                    end_time=pd.Timestamp("22:30"),
                    title="Astronomy observations",
                    room=Room.OUTDOOR,
                    speaker="AstronautiX",
                ),
            ]
        ),
        "18": make_program(
            [
                ProgramEvent(
                    time=pd.Timestamp("09:00"),
                    end_time=pd.Timestamp("09:30"),
                    title="Opening and Registration",
                    room=Room.GRAND_HALL,
                    speaker="KSC and SpaceCon Team",
                ),
                ProgramEvent(
                    time=pd.Timestamp("09:30"),
                    end_time=pd.Timestamp("10:00"),
                    room=Room.AMPHI,
                    title="Introduction to the competition, rules",
                    speaker="KSC",
                ),
                ProgramEvent(
                    time=pd.Timestamp("10:00"),
                    end_time=pd.Timestamp("10:00"),
                    room=Room.AMPHI,
                    title="Competition Starts",
                ),
                ProgramEvent(
                    time=pd.Timestamp("12:30"),
                    end_time=pd.Timestamp("14:00"),
                    room=Room.GRAND_HALL,
                    title="L<sup>a</sup>unch Break",
                ),
                ProgramEvent(
                    time=pd.Timestamp("14:00"),
                    end_time=pd.Timestamp("20:00"),
                    room=Room.GRAND_HALL,
                    title="Teams Interview (20 minutes each)",
                    speaker="Teams",
                ),
                ProgramEvent(
                    time=pd.Timestamp("20:00"),
                    end_time=pd.Timestamp("21:30"),
                    room=Room.GRAND_HALL,
                    title="Dinner Break",
                ),
                ProgramEvent(
                    time=pd.Timestamp("20:00"),
                    end_time=pd.Timestamp("23:59"),
                    title="Astronomy observations",
                    room=Room.OUTDOOR,
                    speaker="AstronautiX",
                ),
            ]
        ),
        "19": make_program(
            [
                ProgramEvent(
                    time=pd.Timestamp("10:00"),
                    end_time=pd.Timestamp("10:00"),
                    title="Mouse and Keyboard down",
                    room=Room.AMPHI,
                ),
                ProgramEvent(
                    time=pd.Timestamp("10:15"),
                    end_time=pd.Timestamp("12:30"),
                    room=Room.PC1,
                    title="Final presentations in front of the jury",
                    speaker="Teams",
                ),
                ProgramEvent(
                    time=pd.Timestamp("10:15"),
                    end_time=pd.Timestamp("12:30"),
                    room=Room.PC2,
                    title="Final presentations in front of the jury",
                    speaker="Teams",
                ),
                ProgramEvent(
                    time=pd.Timestamp("14:00"),
                    end_time=pd.Timestamp("15:00"),
                    room=Room.AMPHI,
                    title="4 finalist teams present in front of the jury",
                ),
                ProgramEvent(
                    time=pd.Timestamp("15:00"),
                    end_time=pd.Timestamp("15:30"),
                    room=Room.AMPHI,
                    title="Winner team is announced",
                ),
                ProgramEvent(
                    time=pd.Timestamp("15:30"),
                    end_time=pd.Timestamp("15:30"),
                    room=Room.GRAND_HALL,
                    title="End of KSPACECONTEST",
                ),
            ]
        ),
    },
    "menus": {
        "main": [
            MenuItem(
                "Tickets",
                icon="fa-solid fa-ticket",
                children=[
                    MenuItem(
                        "16/11 (Opening Cocktail)",
                        url=Url(
                            "https://pretix.eu/spacecon/cocktail-2023",
                            "Join us for the opening cocktail",
                        ),
                        icon="fa-solid fa-ticket",
                    ),
                    MenuItem(
                        "17/11 (Plenary)",
                        url=Url(
                            "https://pretix.eu/spacecon/2023",
                            "Join us for the plenary conference",
                        ),
                        icon="fa-solid fa-ticket",
                    ),
                    MenuItem(
                        "18-19/11 (e-sport)",
                        url=Url(
                            "https://pretix.eu/spacecon/kspacecontest/",
                            "Join us for the KSPACECONTEST",
                        ),
                        icon="fa-solid fa-ticket",
                    ),
                ],
            ),
            MenuItem(
                "About",
                icon="fa-solid fa-circle-info",
                children=[
                    MenuItem(
                        "The Vision",
                        icon="fa-solid fa-circle-info",
                        url=Url(
                            "#about", "Know more about the vision behind the event"
                        ),
                    ),
                    MenuItem(
                        "Reach the venue",
                        icon="fa-solid fa-location-dot",
                        url=Url("#reach-us", "Reach the venue"),
                    ),
                ],
            ),
            MenuItem(
                "Sponsor",
                url=Url("#sponsor", "Check the list of sponsors"),
                icon="fa-solid fa-hand-holding-dollar",
            ),
            MenuItem(
                "Program",
                url=Url("#program", "Check the program of the event"),
                icon="fa-solid fa-book-open",
            ),
            MenuItem(
                "Discover",
                icon="fa-solid fa-magnifying-glass-plus",
                children=[
                    MenuItem(
                        "SpaceConcept",
                        icon="fa-solid fa-rocket",
                        url=Url("#pitch", "Check our pitch contest"),
                    ),
                    MenuItem(
                        "KSPACECONTEST",
                        icon="fa-solid fa-gamepad",
                        url=Url("#ksp", "Join our KSP e-sport competition"),
                    ),
                    MenuItem(
                        "SpaceConference",
                        icon="fa-solid fa-person-chalkboard",
                        url=Url("#lineup", "Check our lineup"),
                    ),
                ],
            ),
        ]
    },
    "sponsors": {
        SponsorLevel(name="Neptune", color="[#7D1758]"): [
            SponsorItem(
                logo=Image(
                    src="./img/polytechnique.svg",
                    alt="Logo of École polytechnique",
                ),
                url=Url(
                    href="https://polytechnique.edu",
                    title="École polytechnique website",
                ),
            )
        ],
        SponsorLevel(name="Earth", color="spacecon-redish"): [
            SponsorItem(
                logo=Image(src="./img/omen-black.svg", alt="Logo of OMEN"),
                url=Url(
                    href="https://www.omen.com/fr/fr.html",
                    title="Omen website",
                ),
            )
        ],
        SponsorLevel(name="Venus", color="spacecon-magenta"): [
            SponsorItem(
                logo=Image(src="./img/euro2moon.svg", alt="Logo of Euro2moon"),
                url=Url(
                    href="https://euro2moon.com/",
                    title="Euro2moon Website",
                ),
            ),
            SponsorItem(
                logo=Image(src="./img/airliquide.svg", alt="Logo of Airliquide"),
                url=Url(
                    href="https://airliquide.com/",
                    title="Airliquide Website",
                ),
            ),
        ],
        SponsorLevel(name="Venus", color="spacecon-magenta"): [
            SponsorItem(
                logo=Image(src="./img/euro2moon.svg", alt="Logo of Euro2moon"),
                url=Url(
                    href="https://euro2moon.com/",
                    title="Euro2moon Website",
                ),
            ),
            SponsorItem(
                logo=Image(src="./img/airliquide.svg", alt="Logo of Airliquide"),
                url=Url(
                    href="https://airliquide.com/",
                    title="Airliquide Website",
                ),
            ),
        ],
        SponsorLevel(name="Mars", color="spacecon-violet"): [
            SponsorItem(
                logo=Image(
                    src="./img/partners/16_Incuballiance.svg",
                    alt="Logo of Incuballiance",
                ),
                url=Url(
                    href="https://incuballiance.fr/",
                    title="Incuballiance",
                ),
            ),
        ],
    },
    "lineup": lineup,
}

ksp: dict[str, Any] = {
    "DESCRIPTION": (
        "Register to KSPACECONTEST the first Kerbal Space Program e-sport contest"
    ),
    "TITLE": "Register your team to KSPACECONTEST",
}

ksp_success: dict[str, Any] = {
    "DESCRIPTION": "Thanks for registering to KSPACECONTEST!",
    "TITLE": "Thanks for registering to KSPACECONTEST",
}

pages = {"index": index}
