#!/usr/bin/env python
import argparse
from typing import Any, Sequence

import tomli


def as_tuple(elements: list[dict[str, Any]]) -> list[tuple[str, Any]]:
    return sorted((k, v) for d in elements for k, v in d.items())


def main(argv: Sequence[str] | None = None):
    parser = argparse.ArgumentParser()
    parser.add_argument("filenames", nargs="*", help="Filenames to check.")
    args = parser.parse_args(argv)

    for filename in args.filenames:
        for to_check in set(args.filenames) - {filename}:
            try:
                with open(filename, mode="rb") as fp1, open(to_check, mode="rb") as fp2:
                    content1 = tomli.load(fp1)
                    content2 = tomli.load(fp2)

                    assert (
                        len(
                            set(as_tuple(content1["d1_databases"])).intersection(
                                as_tuple(content2["d1_databases"])
                            )
                        )
                        > 0
                    )

                retval = 0

            except (tomli.TOMLDecodeError, AssertionError) as exc:
                print(f"{filename}: {exc}")
                retval = 1
    return retval


if __name__ == "__main__":
    raise SystemExit(main())
