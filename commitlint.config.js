module.exports = {
  extends: ["gitmoji"],
  rules: {
    "subject-empty": [2, "always"],
    "type-empty": [2, "always"],
  },
};
