from pathlib import Path
from typing import Any, MutableMapping

from jinja2 import Environment, FileSystemLoader, Template


def flatten_filter(iterable):
    items = []
    for item in iterable:
        if item.children is not None:
            items += [item for item in flatten_filter(item.children)]
        else:
            items.append(item)
    return items


def make_set_filter(iterable):
    return set([e for e in iterable])


def is_break_filter(program_subentries):
    for subentry in program_subentries:
        if "Break" in subentry.title:
            return True
    return False


class PageManager:
    DEFAULT_TEMPLATE_EXTENSION = "html.j2"

    _output_path: Path

    def __init__(self, layout_path, output_path, assets_path, global_ctx) -> None:
        self.layout_path = layout_path
        self.output_path = output_path
        self.assets_path = assets_path
        self.global_ctx = global_ctx

        self._env = Environment(loader=FileSystemLoader(self.layout_path))
        self._env.filters["flatten_menu"] = flatten_filter
        self._env.filters["make_set"] = make_set_filter
        self._env.filters["is_break"] = is_break_filter

    @property
    def output_path(self) -> Path:
        if not (self._output_path.exists()):
            self._output_path.mkdir()
        return self._output_path

    @output_path.setter
    def output_path(self, path: Path) -> None:
        self._output_path = path

    def get_template(self, page: str) -> Template:
        return self._env.get_template(f"{page}.{self.DEFAULT_TEMPLATE_EXTENSION}")

    def render_page(self, page: str, ctx: MutableMapping[str, Any]) -> None:
        """Render the provided page using the corresponding template

        Parameters
        ----------
        page
            The page to render
        ctx
            A dictionary containing variables to pass to the jinja template
        """
        with open(self.output_path / f"{page}.html", "w") as f:
            f.write(self.get_template(page).render(**(ctx | self.global_ctx)))
