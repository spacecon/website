import json
import re

# import sys
from dataclasses import dataclass
from pathlib import Path
from typing import Any

from google.oauth2 import service_account
from googleapiclient.discovery import build
from pandas import Timestamp
from rich.status import Status


@dataclass
class Partner:
    name: str
    img_path: str


SCOPES = [
    "https://www.googleapis.com/auth/admin.directory.user",
    "https://www.googleapis.com/auth/drive",
]

CREDS = service_account.Credentials.from_service_account_file(
    "credentials.json", scopes=SCOPES, subject="rdb@spacecon.io"
)


def get_users() -> list[dict[str, Any]]:
    service = build("admin", "directory_v1", credentials=CREDS)

    # Call the Admin SDK Directory API
    results = (
        service.users().list(customer="my_customer", orderBy="givenName").execute()
    )
    users = results.get("users", [])

    if not users:
        raise ValueError("No users in the domain.")

    return users


PARTNER_FILE_REGEX = re.compile(r"(?P<id>\d)+_(?P<partner>.*)\..*")


def get_partners(assets_path: Path) -> list[Partner]:
    service = build("drive", "v3", credentials=CREDS)

    res = (
        service.files()
        .list(
            driveId="0AC5OsoMiYKvNUk9PVA",
            corpora="drive",
            q="'1XcNvLEfwbZuZzE4FRF02yJzkW3ipvIwd' in parents",
            includeItemsFromAllDrives=True,
            supportsAllDrives=True,
            fields="nextPageToken, files(id, name, trashed, modifiedTime)",
        )
        .execute()
    )

    partners: list[Partner] = []
    img_dir = assets_path / "img" / "partners"
    img_dir.mkdir(exist_ok=True, parents=True)

    if (cache_path := assets_path / "partners.json").exists():
        cache = json.loads((assets_path / "partners.json").read_text())
    else:
        cache = {}

    with Status("Download partners images..."):
        for f in sorted(res["files"], key=lambda f: f["name"]):
            if f["trashed"]:
                continue
            changed = Timestamp(f["modifiedTime"]) >= Timestamp(
                cache.get(f["name"], "1970-01-01 00:00+00:00")
            )

            m = PARTNER_FILE_REGEX.search(f["name"])

            if m is None:
                raise ValueError("match is none")

            _path = img_dir / f["name"]
            if changed:
                print(f"{f['name']} will be downloaded...")
                _path.write_bytes(service.files().get_media(fileId=f["id"]).execute())
                cache[f["name"]] = str(Timestamp.now(tz="UTC"))

            partners.append(
                Partner(
                    name=" ".join(m.group("partner").split("_")),
                    img_path=f"./img/partners/{f['name']}",
                )
            )

    with Status("Removing deleted partners images..."):
        names = set([f["name"] for f in res["files"] if not (f["trashed"])])
        current = set([f.name for f in img_dir.glob("*")])

        for to_remove in current - names:
            del cache[to_remove]
            (img_dir / to_remove).unlink()

    cache_path.write_text(json.dumps(cache))
    return partners


# if __name__ == "__main__":
# get_users()
# get_partners(Path(sys.argv[1]))
