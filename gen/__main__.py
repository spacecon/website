import importlib
import importlib.resources
import importlib.util
import json
import logging
import os
import shutil
import subprocess
import sys
import threading
from http.server import SimpleHTTPRequestHandler, ThreadingHTTPServer
from pathlib import Path
from types import ModuleType
from typing import Any

import click
from watchdog.observers import Observer

from .google import Partner, get_partners, get_users
from .page_manager import PageManager
from .watchdog import WatchDog

LOGGER = logging.getLogger(__name__)

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(message)s", datefmt="%Y-%m-%d %H:%M:%S"
)


def _generate_menu_state_file(menus: dict[str, Any], output_path: Path) -> None:
    for menu, entries in menus.items():
        state = {}
        for entry in entries:
            if entry.children is None:
                continue
            state[entry.title.lower()] = False

        with open(output_path / f"{menu}.json", "w") as f:
            json.dump(state, f)


def _import_from_file(file_path: Path) -> ModuleType:
    module_name = file_path.stem
    spec = importlib.util.spec_from_file_location(module_name, file_path)
    if spec is None:
        raise ValueError("spec is None")
    module = importlib.util.module_from_spec(spec)
    sys.modules[module_name] = module
    loader = spec.loader
    if loader is None:
        raise ValueError("loader is None")
    loader.exec_module(module)
    return module


def filesystem_watch(conf: ModuleType, serve: bool, partners: list[Partner]):
    shutil.copytree(conf.ASSETS_PATH, conf.OUTPUT_PATH, dirs_exist_ok=True)

    # Fontawesome sync
    for k, v in conf.NODE_MODULES_SYNC.items():
        print(k, v)
        shutil.copytree(k, conf.OUTPUT_PATH / v, dirs_exist_ok=True)

    # Enrich global context
    branch = os.environ.get("CI_COMMIT_REF_NAME", "")
    if branch == "main":
        branch = ""

    conf.global_ctx |= {  # type: ignore[attr-defined]
        "users": get_users(),
        "partners": partners,
        "branch": branch,
    }

    page_manager = PageManager(
        layout_path=conf.LAYOUT_PATH,
        output_path=conf.OUTPUT_PATH,
        assets_path=conf.ASSETS_PATH,
        global_ctx=conf.global_ctx
        if not serve
        else conf.global_ctx | conf.dev_ctx_override,
    )

    for page, ctx in conf.pages.items():
        if menus := ctx.get("menus"):
            _generate_menu_state_file(menus, conf.OUTPUT_PATH)
        page_manager.render_page(page, ctx=ctx)

    with importlib.resources.as_file(
        importlib.resources.files(__package__) / "tailwind-style.css"
    ) as tw_file:
        args = [
            conf.NPX,
            "tailwindcss",
            "-c",
            conf.TAILWIND_CONFIG_FILE,
            "-i",
            tw_file,
            "-o",
            conf.OUTPUT_PATH / "css" / "style.css",
        ]

        if not serve:
            args.append("--minify")

        subprocess.run(args, stdout=subprocess.PIPE)


@click.command()
@click.option("-c", "--conf", help="Directory to a Python configuration file", type=str)
@click.option("-p", "--port", help="Port serving the output", type=int, default=8000)
@click.option(
    "--serve/--no-serve",
    help="Flag to activate / deactivate the http server",
    default=False,
)
def main(conf: str, port: int, serve: bool):
    config = _import_from_file(Path(conf))

    partners = get_partners(config.OUTPUT_PATH)

    def _handler():
        return filesystem_watch(config, serve, partners)

    _handler()

    class Handler(SimpleHTTPRequestHandler):
        def __init__(self, *args, directory=None, **kwargs):
            super().__init__(*args, directory=config.OUTPUT_PATH, **kwargs)

    obs = Observer()
    for path in (config.LAYOUT_PATH, config.ASSETS_PATH):
        obs.schedule(WatchDog(_handler), path=path, recursive=True)

    if serve:
        with ThreadingHTTPServer(("", port), Handler) as httpd:
            LOGGER.info("Serving on port http://localhost:%s...", port)
            thread = threading.Thread(target=httpd.serve_forever)
            thread.daemon = True
            thread.start()

            obs.start()

            try:
                while obs.is_alive():
                    obs.join(1)
            finally:
                LOGGER.info("Closing...")
                obs.stop()
                obs.join()


if __name__ == "__main__":
    main()
