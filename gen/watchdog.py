import logging
from typing import Callable

from watchdog.events import DirModifiedEvent, FileSystemEvent, FileSystemEventHandler

LOGGER = logging.getLogger(__name__)


class WatchDog(FileSystemEventHandler):
    def __init__(self, handler: Callable[[], None]) -> None:
        self._handler = handler

    def on_modified(self, event: FileSystemEvent):
        super().on_modified(event)

        if isinstance(event, DirModifiedEvent):
            self._handler()
