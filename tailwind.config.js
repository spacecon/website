/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["public/**/*.html", "workers/src/templates/*.j2"],
  theme: {
    extend: {
      colors: {
        "spacecon-magenta": "#660F59",
        "spacecon-redish": "#C5103E",
        "spacecon-violet": "#3B1C60",
        "spacecon-white": "#FEDEE5",
      },
      backgroundSize: {
        mobile: "220%",
      },
      backgroundImage: {
        skyline: "url('../img/skyline.svg')",
      },
      animation: {
        "infinite-scroll": "infinite-scroll 20s linear infinite",
      },
      keyframes: {
        "infinite-scroll": {
          from: { transform: "translateX(0)" },
          to: { transform: "translateX(-100%)" },
        },
      },
    },
    fontFamily: {
      sans: ["Inter", "ui-sans-serif"],
      mono: ["Inconsolata"],
    },
  },
  plugins: [],
};
