if [ -z ${DEV+x} ]; then
    ENDPOINT="https://ks-subscribe.rdb-c41.workers.dev"
else
    ENDPOINT="http://127.0.0.1:8787"
fi

echo "ENDPOINT: ${ENDPOINT}"

curl "${ENDPOINT}/ksp/register" -X POST \
    -H 'HX-Request: true' \
    -H 'HX-Current-URL: http://localhost:8000/ksp.html' \
    -H 'Content-Type: application/x-www-form-urlencoded' \
    -H 'Origin: http://localhost:8000' \
    -H 'Cache-Control: no-cache, no-store' \
    --data-urlencode 'First Name=Ruben' \
    --data-urlencode 'First Name=Pierre' \
    --data-urlencode 'Last Name=Di Battista' \
    --data-urlencode 'Last Name=Cordesse' \
    --data-urlencode 'Nickname=' \
    --data-urlencode 'Nickname=' \
    --data-urlencode 'Pronouns=' \
    --data-urlencode 'Pronouns=' \
    --data-urlencode "email=rdb@iso.${RANDOM}" \
    --data-urlencode "email=pierre@test.${RANDOM}" \
    --data-urlencode 'team=SpaceCon' \
    --data-urlencode 'game-rules=' \
    --data-urlencode 'privacy=' \
    -i
