SELECT p.*,t.name as team_name, s.paid
FROM player as p
JOIN team as t ON t.name = p.team
JOIN checkout_session as s ON t.checkout_session_id = s.id;
