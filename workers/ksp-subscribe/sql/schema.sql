DROP TABLE IF EXISTS player;
DROP TABLE IF EXISTS team;
DROP TABLE IF EXISTS checkout_session;

CREATE TABLE player (
    email TEXT PRIMARY KEY,
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL,
    nickname TEXT,
    pronouns TEXT,
    team TEXT,

    CONSTRAINT fk_team
        FOREIGN KEY (team)
        REFERENCES team(name)
        ON DELETE CASCADE
);

CREATE TABLE team (
    name TEXT PRIMARY KEY UNIQUE,
    checkout_session_id INTEGER NOT NULL,

    CONSTRAINT fk_checkout_session
        FOREIGN KEY(checkout_session_id)
        REFERENCES checkout_session(id)
        ON DELETE CASCADE

);

CREATE TABLE checkout_session (
    id TEXT PRIMARY KEY,
    created DATETIME NOT NULL DEFAULT current_timestamp,
    paid DATETIME,
    cancel_uuid TEXT UNIQUE NOT NULL
);
