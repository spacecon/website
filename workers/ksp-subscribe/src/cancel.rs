use minijinja::Environment;
use worker::*;

use crate::register::stripe::SESSION_ENDPOINT;

async fn expire_session(client_key: &String, session_id: &String) -> Result<()> {
    let client = reqwest::Client::new();

    let mut url = Url::parse(SESSION_ENDPOINT)?;

    {
        let mut path = url.path_segments_mut().unwrap();
        path.push(&session_id);
        path.push("expire");
    }

    match client
        .post(url)
        .basic_auth::<String, String>(client_key.to_string(), None)
        .send()
        .await
    {
        Ok(r) => {
            if r.status() == 200 {
                console_debug!("{:?}", r);
                return Ok(());
            }
            console_error!("{:?}", r);
            return Err(Error::Internal(
                "Couldn't expire the checkout session".into(),
            ));
        }
        Err(e) => return Err(Error::Internal(e.to_string().into())),
    }
}

pub async fn handle(req: Request, ctx: RouteContext<&Environment<'_>>) -> Result<Response> {
    let d1 = match ctx.env.d1("db") {
        Err(e) => {
            console_error!("{}", e.to_string());
            return Response::error(e.to_string(), 500);
        }
        Ok(d) => d,
    };

    if let Some(cancel_uuid) = ctx.param("id") {
        let origin = match req.url() {
            Ok(u) => worker::Url::parse(u.as_str())?,
            Err(e) => return Response::error(e.to_string(), 400),
        };

        let redirect = match origin.query_pairs().into_iter().find(|x| x.0 == "redirect") {
            Some(r) => Url::parse(&r.1)?,
            None => return Response::error("redirect not provided", 400),
        };

        match query!(
            &d1,
            "SELECT id FROM checkout_session WHERE cancel_uuid = ?",
            &cancel_uuid
        )?
        .first::<String>(Some("id"))
        .await?
        {
            Some(cs) => {
                expire_session(&ctx.env.secret("STRIPE_CLIENT_KEY")?.to_string(), &cs).await?
            }
            None => return Response::error("Can't find a valid checkout session id", 400),
        }

        return Response::redirect(redirect);
    }

    Response::error("Invalid request", 400)
}
