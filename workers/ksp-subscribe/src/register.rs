use minijinja::Environment;
use worker::*;

mod db;
mod form;
pub mod stripe;

macro_rules! cast {
    ($target: expr, $pat: path) => {{
        if let $pat(a) = $target {
            // #1
            a
        } else {
            panic!("mismatch variant when cast to {}", stringify!($pat)); // #2
        }
    }};
}

pub async fn handle(mut req: Request, ctx: RouteContext<&Environment<'_>>) -> Result<Response> {
    let d1 = match ctx.env.d1("db") {
        Err(e) => {
            console_error!("{}", e.to_string());
            return Response::error(e.to_string(), 500);
        }
        Ok(d) => d,
    };
    let jinja_env = ctx.data;
    let form_data = req.form_data().await?;

    match form::validate_form(&form_data, &d1).await {
        Err(e) => {
            let err_string = e.to_string();
            let errors = err_string.split(",").collect::<Vec<&str>>();
            console_error!("{:?}", errors);
            let tmpl = jinja_env.get_template("error").unwrap();
            let rsp = match tmpl.render(minijinja::context! {errors => errors}) {
                Err(e) => {
                    console_error!("{}", e.to_string());
                    return Response::error(e.to_string(), 500);
                }
                Ok(s) => s,
            };
            return Response::from_html(rsp);
        }
        Ok(()) => (),
    }

    let players = form::Player::from_form_data(&form_data);
    let team = cast!(form_data.get("team").unwrap(), FormEntry::Field).to_string();
    let checkout = stripe::create_checkout(
        &ctx.env.secret("STRIPE_CLIENT_KEY")?.to_string(),
        &ctx.env.var("STRIPE_PRICE_ID")?.to_string(),
        &Url::parse(req.headers().get("Origin")?.unwrap_or_default().as_str()).unwrap(),
        &req.url()?,
    )
    .await?;
    match db::insert_records(&players, &team, &checkout, &d1).await {
        Ok(_result) => (),
        Err(e) => {
            console_error!("{:?}", e.to_string());
            return Response::error(e.to_string(), 500);
        }
    }
    let mut headers = Headers::new();
    headers.set("HX-Redirect", checkout.url.as_str())?;
    Ok(Response::empty()?.with_headers(headers))
}
