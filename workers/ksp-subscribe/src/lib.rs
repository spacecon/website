use minijinja;
use worker::*;

mod cancel;
mod register;

const ALLOWED_ORIGINS: &[&str] = &[
    "http://localhost:8000",
    "https://dev.spacecon.io",
    "https://spacecon.io",
];

#[event(fetch)]
async fn main(req: Request, env: Env, _ctx: Context) -> Result<Response> {
    let mut jinja_env = minijinja::Environment::new();
    jinja_env
        .add_template("summary", include_str!("templates/summary.html.j2"))
        .unwrap();
    jinja_env
        .add_template("error", include_str!("templates/error.html.j2"))
        .unwrap();

    let router = Router::with_data(&jinja_env);

    // Handle multiple allowed CORS
    let mut origin = req.headers().get("Origin")?.unwrap_or_default();

    if !ALLOWED_ORIGINS.contains(&origin.as_str()) {
        origin = ALLOWED_ORIGINS[0].to_string();
    }

    let cors = Cors::default()
        .with_max_age(86400)
        .with_origins(vec![origin])
        .with_methods(vec![
            Method::Get,
            Method::Head,
            Method::Post,
            Method::Options,
        ])
        .with_allowed_headers(vec![
            "Content-type",
            "hx-current-url",
            "hx-request",
            "hx-target",
        ])
        .with_exposed_headers(vec!["hx-redirect"]);

    router
        .post_async("/ksp/register", register::handle)
        .options_async(
            "/ksp/register",
            |_req, _ctx| async move { Response::empty() },
        )
        .get_async("/ksp/cancel/:id", cancel::handle)
        .run(req, env)
        .await
        .unwrap()
        .with_cors(&cors)
}
