use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use uuid::Uuid;
use worker::{Error, Result, Url};

pub const SESSION_ENDPOINT: &str = "https://api.stripe.com/v1/checkout/sessions";

#[derive(Debug, Deserialize, Serialize)]
struct CheckoutSession {
    id: String,
    url: String,
}

pub struct Checkout {
    pub id: String,
    pub url: String,
    pub cancel_uuid: String,
}

pub async fn create_checkout(
    client_key: &String,
    price_id: &String,
    origin: &Url,
    req_endpoint: &Url,
) -> Result<Checkout> {
    let client = reqwest::Client::new();

    let uuid = Uuid::new_v4().to_string();
    let success_url = origin.join("/ksp-success.html").unwrap();
    let mut cancel_url = req_endpoint
        .join(format!("/ksp/cancel/{uuid}").as_str())
        .unwrap();

    cancel_url.set_query(Some(format!("redirect={origin}").as_str()));

    let params = HashMap::from([
        ("success_url", success_url.as_str()),
        ("cancel_url", cancel_url.as_str()),
        ("mode", "payment"),
        ("line_items[0][price]", price_id.as_str()),
        ("line_items[0][quantity]", "1"),
    ]);

    let req = client
        .post(SESSION_ENDPOINT)
        .basic_auth::<String, String>(client_key.to_string(), None)
        .form(&params);

    let rsp = match req.send().await {
        Err(e) => return Err(Error::RustError(e.to_string())),
        Ok(r) => r,
    };

    let rsp_json: CheckoutSession = rsp.json().await.unwrap();
    return Ok(Checkout {
        id: rsp_json.id,
        url: rsp_json.url,
        cancel_uuid: uuid,
    });
}
