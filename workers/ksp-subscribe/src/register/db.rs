use super::{form::Player, stripe::Checkout};
use worker::{console_debug, console_error, query, D1Database, D1PreparedStatement, Result};

pub async fn insert_records(
    players: &(Player, Player),
    team: &str,
    checkout: &Checkout,
    d1: &D1Database,
) -> Result<()> {
    let queries: Vec<D1PreparedStatement> = vec![
        query!(&d1, "INSERT INTO checkout_session(id, cancel_uuid) VALUES (?, ?)", &checkout.id, &checkout.cancel_uuid).unwrap(),
        query!(&d1, "INSERT INTO team(name, checkout_session_id) VALUES (?,?)", &team, &checkout.id).unwrap(),
        query!(&d1, "INSERT INTO player(email, first_name, last_name, nickname, pronouns, team) VALUES (?,?,?,?,?,?)",
        &players.0.email.to_string(),
        &players.0.first_name,
        &players.0.last_name,
        &players.0.nickname,
        &players.0.pronouns,
        &team,
        ).unwrap(),
        query!(&d1, "INSERT INTO player(email, first_name, last_name, nickname, pronouns, team) VALUES (?,?,?,?,?,?)",
        &players.1.email.to_string(),
        &players.1.first_name,
        &players.1.last_name,
        &players.1.nickname,
        &players.1.pronouns,
        &team,
        ).unwrap(),
    ];

    match d1.batch(queries).await {
        Ok(_a) => (),
        Err(e) => {
            console_error!("{}", e.to_string());
            return Err(worker::Error::RustError(e.to_string()));
        }
    }
    return Ok(());
}
