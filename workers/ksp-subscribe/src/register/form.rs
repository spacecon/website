use email_address::EmailAddress;
use serde::Serialize;
use std::collections::{HashMap, HashSet};
use std::str::FromStr;
use worker::*;

const MANDATORY_FIELDS: &[&str] = &["First Name", "Last Name", "email", "team"];
const OPTIONAL_FIELDS: &[&str] = &["Nickname", "Pronouns"];

#[derive(Debug, Serialize)]
pub struct Player {
    pub first_name: String,
    pub last_name: String,
    pub email: EmailAddress,
    pub nickname: Option<String>,
    pub pronouns: Option<String>,
}

impl Player {
    pub fn from_form_data(form_data: &FormData) -> (Player, Player) {
        let mut fields: HashMap<&str, Vec<String>> = HashMap::new();
        for field in [MANDATORY_FIELDS, OPTIONAL_FIELDS].concat() {
            fields.insert(
                field,
                form_data
                    .get_all(field)
                    .unwrap_or_default()
                    .into_iter()
                    .map(|entry| match entry {
                        FormEntry::Field(s) => s,
                        _ => "".to_string(),
                    })
                    .collect(),
            );
        }

        (
            Player {
                first_name: fields
                    .get("First Name")
                    .unwrap()
                    .first()
                    .unwrap()
                    .to_string(),
                last_name: fields
                    .get("Last Name")
                    .unwrap()
                    .first()
                    .unwrap()
                    .to_string(),
                email: EmailAddress::from_str(fields.get("email").unwrap().first().unwrap())
                    .unwrap(),
                nickname: fields.get("Nickname").unwrap().first().cloned(),
                pronouns: fields.get("Pronouns").unwrap().first().cloned(),
            },
            Player {
                first_name: fields
                    .get("First Name")
                    .unwrap()
                    .last()
                    .unwrap()
                    .to_string(),
                last_name: fields.get("Last Name").unwrap().last().unwrap().to_string(),
                email: EmailAddress::from_str(fields.get("email").unwrap().last().unwrap())
                    .unwrap(),
                nickname: fields.get("Nickname").unwrap().last().cloned(),
                pronouns: fields.get("Pronouns").unwrap().last().cloned(),
            },
        )
    }
}

pub async fn validate_form(form_data: &FormData, d1: &D1Database) -> Result<()> {
    let mut errors: HashSet<String> = HashSet::new();

    if !form_data.has("game-rules") || !form_data.has("privacy") {
        errors.insert("All checkboxes must be filled!".into());
    }

    // Validate mandatory fields
    for field in MANDATORY_FIELDS {
        if !form_data.has(field) {
            errors.insert(format!("Mandatory field {field} not present"));
        }

        let values: Vec<String> = form_data
            .get_all(field)
            .unwrap_or_default()
            .into_iter()
            .map(|entry| match entry {
                FormEntry::Field(s) => s,
                FormEntry::File(f) => f.name(),
            })
            .collect();

        if field == &"email" {
            for email in &values[..] {
                if !EmailAddress::is_valid(&email) {
                    errors.insert(format!("{email} is not a valid email!"));
                }

                let stmt = d1.prepare("SELECT email FROM player  WHERE email = ?1");
                let query = stmt.bind(&[email.into()])?;
                match query.first::<String>(None).await? {
                    Some(email) => {
                        errors.insert(format!("{email} already registered"));
                        ()
                    }
                    None => (),
                }
            }
        }

        for value in &values[..] {
            if value.is_empty() {
                errors.insert(format!("Fill all mandatory fields ({field})"));
            }
        }

        let max_len = match field {
            &"team" => 1,
            &&_ => 2,
        };

        if values.len() > max_len {
            errors.insert(format!(
                "Invalid number of entries for {field}, max: {max_len}"
            ));
        }
    }

    if errors.len() > 0 {
        return Err(Error::RustError(
            errors.into_iter().collect::<Vec<String>>().join(","),
        ));
    }

    Ok(())
}
