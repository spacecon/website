import { Router } from '@tsndr/cloudflare-worker-router';
import { Stripe } from 'stripe';

export type Env = {
	db: D1Database;
};

const router = new Router<Env>();

router.post('/webhook', async ({ env, ctx, req }) => {
	const stripe = new Stripe(env.STRIPE_CLIENT_KEY, { apiVersion: '2023-08-16' });
	const endpoint_secret = env.STRIPE_WEBHOOK_SIGN_KEY;
	const payload = await req.raw.text();
	const sig = req.headers.get('stripe-signature');

	let event;

	try {
		event = await stripe.webhooks.constructEventAsync(payload, sig, endpoint_secret);
	} catch (err: any) {
		console.error(err);
		return new Response(`Webhook Error: ${err.message}`, { status: 400 });
	}

	// Handle the checkout.session.completed event
	if (event.type === 'checkout.session.completed') {
		// Retrieve the session. If you require line items in the response, you may include them by expanding line_items.
		const session = await stripe.checkout.sessions.retrieve(event.data.object.id);
		console.info('checkout.session.completed');
		console.debug(session);

		// Fulfill the purchase...
		await fulfillOrder(session.id, env);
	}

	if (event.type === 'checkout.session.expired') {
		// Retrieve the session. If you require line items in the response, you may include them by expanding line_items.
		const session = await stripe.checkout.sessions.retrieve(event.data.object.id);
		console.info('checkout.session.expired');
		console.debug(session);

		// Fulfill the purchase...
		await expireOrder(session.id, env);
	}

	return new Response(null, { status: 200 });
});

export default {
	async fetch(request: Request, env: Env, ctx: ExecutionContext) {
		return router.handle(request, env, ctx);
	},
};

async function fulfillOrder(sessionId: string, env: Env) {
	logError('fulfill', await env.db.prepare('UPDATE checkout_session SET paid = current_timestamp WHERE id = ?').bind(sessionId).run());
}

async function expireOrder(sessionId: string, env: Env) {
	logError('expire', await env.db.prepare('DELETE FROM checkout_session WHERE id = ?').bind(sessionId).run());
}

function logError(tag: string, result: D1Result) {
	if (!result.success) {
		console.error(`${tag} failed!`);
		console.debug(result.meta);
	}
}
