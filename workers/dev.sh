#!/bin/zsh

SESSIONNAME="wrangler"
tmux has-session -t $SESSIONNAME &>/dev/null

if [ $? != 0 ]; then
    tmux new-session -s $SESSIONNAME -d
    tmux send-keys 'stripe listen --forward-to localhost:8888/webhook' C-m
    tmux split-window -v
    tmux send-keys 'cd stripe-fulfill && npx wrangler dev --port 8888' C-m
    tmux split-window -h
    tmux send-keys 'cd ksp-subscribe && npx wrangler dev --port 8787' C-m
    tmux split-window -h -U
fi

tmux attach -t $SESSIONNAME
